package by.sportshop.main;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

public class Verificator{
	private SchemaFactory fact;
	private String sConst= XMLConstants.W3C_XML_SCHEMA_NS_URI;
	private File xmlF;
	private File xsdF;
	private String result;
 
	public Verificator (String sXmlF, String sXsdF) { 
		xmlF=new File(sXmlF);
		xsdF=new File(sXsdF);
		fact = SchemaFactory.newInstance(sConst);
		result="";
	}
	
	public boolean doValidate (){
		result="File "+xmlF.getName() + " has been validated by scheme "+xsdF.getName()+".";
		try {
			fact.newSchema(xsdF).newValidator().validate(new StreamSource(xmlF));
		} catch (SAXException e) {
			result="File "+xmlF.getName() + " has NOT been validated by scheme "+xsdF.getName()+". "+e.getMessage();
			return false;
		} catch (IOException e) {
			result="Files are not ready. "+e.getMessage();
			return false;
		}
		return true;
	}
	
	public String getResult (){
		return result;
	}
}